﻿using Microsoft.EntityFrameworkCore;

using sportsData.api.Models;
using sportsData.Common.Eventing;
using sportsData.Common.Eventing.Providers;

namespace sportsData.api.Data
{
    public class ApiDataContext : DbContext, IProvideEventingData
    {
        public ApiDataContext(DbContextOptions<ApiDataContext> options) :
            base(options) { }

        public DbSet<City> Cities { get; set; }

        public DbSet<ImageData> ImageData { get; set; }

        public DbSet<State> States { get; set; }

        public DbSet<Venue> Venues { get; set; }

        public DbSet<OutgoingEvent> OutgoingEvents { get; set; }

        public DbSet<IncomingEvent> IncomingEvents { get; set; }
    }
}