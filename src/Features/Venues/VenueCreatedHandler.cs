﻿using MediatR;

using sportsData.Events.Venue;

using System;
using System.Threading;
using System.Threading.Tasks;

namespace sportsData.api.Features.Venues
{
    public class VenueCreatedHandler : IRequestHandler<VenueCreatedHandler.Event>
    {
        public class Event : VenueCreatedEvent, IRequest
        {
            public Event(Guid correlationId, Guid causationId)
                : base(correlationId, causationId) { }
        }

        public async Task<Unit> Handle(Event request, CancellationToken cancellationToken)
        {
            await Task.Delay(100, cancellationToken);
            return Unit.Value;
        }
    }
}