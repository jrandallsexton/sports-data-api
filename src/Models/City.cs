﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace sportsData.api.Models
{
    public class City : ModelBase
    {
        public State State { get; set; }
        public string Name { get; set; }
    }
}