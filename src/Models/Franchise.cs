﻿using System.Collections.Generic;

namespace sportsData.api.Models
{
    public class Franchise : ModelBase
    {
        public string Name { get; set; } // Tigers
        public string Nickname { get; set; } // LSU
        public string Abbreviation { get; set; } // LSU
        public string FullName { get; set; } // LSU Tigers
        public string ShortName { get; set; } // Tigers
        public string Color { get; set; } // 22005c
        public List<ImageData> Logos { get; set; }
    }
}
