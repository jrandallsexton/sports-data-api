﻿
namespace sportsData.api.Models
{
    public class ImageData
    {
        public string Href { get; set; }
        public long Width { get; set; }
        public long Height { get; set; }
        public string Alt { get; set; }
        public string Rel { get; set; }
    }
}