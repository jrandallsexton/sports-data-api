﻿using System;

namespace sportsData.api.Models
{
    public abstract class ModelBase
    {
        public int Id { get; set; }
        public DateTime Created { get; set; }
        public int CreatedBy { get; set; }
        public DateTime? Modified { get; set; }
        public int? ModifiedById { get; set; }
        public DateTime? Deleted { get; set; }
        public int? DeletedById { get; set; }
    }
}