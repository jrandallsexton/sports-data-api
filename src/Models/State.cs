﻿namespace sportsData.api.Models
{
    public class State : ModelBase
    {
        public int CountryId { get; set; }
        public string Name { get; set; }
        public string Abbreviation { get; set; }
    }
}