﻿using System.Collections.Generic;

namespace sportsData.api.Models
{
    public class Venue : ModelBase
    {
        public string Name { get; set; }
        public int Capacity { get; set; } // TODO: At some point break this out to VenueCapacity; would allow us to show changes over time
        public bool IsGrass { get; set; }
        public bool IsIndoor { get; set; }
        public List<ImageData> ImageData { get; set; }
    }
}