using MediatR;

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;

using sportsData.api.Features.Venues;
using sportsData.Common.Eventing.Consumers;
using sportsData.Common.Eventing.Publishers;
using sportsData.Common.ExtensionMethods;
using sportsData.Events.Venue;

using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace sportsData.api
{
    public class Startup
    {
        private readonly IWebHostEnvironment _hostingEnvironment;
        private readonly IConfiguration _configuration;

        public Startup(IConfiguration configuration, IWebHostEnvironment environment)
        {
            _configuration = configuration;
            _hostingEnvironment = environment;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddSwaggerGen(g =>
            {
                g.SwaggerDoc("v1", new OpenApiInfo()
                {
                    Title = "sportData-api",
                    Version = "v1"
                });
            });

            if (_hostingEnvironment.IsDevelopment()) return;
            services.AddHostedService<EventConsumerService>();
            services.AddHostedService<EventPublisherService>();
            services.AddSingleton(RegisterEventHandlers);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(i =>
                {
                    i.SwaggerEndpoint("/swagger/v1/swagger.json", "sportsData-api v1");
                });
            }

            //app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }

        private static Dictionary<string, Func<string, Task>> RegisterEventHandlers(IServiceProvider serviceProvider)
        {
            return new Dictionary<string, Func<string, Task>>()
            {
                {
                    nameof(VenueCreatedEvent),
                    async (message) =>
                    {
                        var incomingEvent = message.FromJson<VenueCreatedHandler.Event>();
                        using var scope = serviceProvider.CreateScope();
                        var mediator = scope.ServiceProvider.GetService<IMediator>();
                        await mediator.Send(incomingEvent);
                    }
                }
            };
        }
    }
}